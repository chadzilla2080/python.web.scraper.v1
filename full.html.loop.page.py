import requests

url =  "http://finance.yahoo.com/quote/AAPL?p=AAPL"

response = requests.get(url)

# New dictionary, this will hold all of the data and get moved to a Panda
#data{}

# Transform from just a list to a dictionary
Indicators = {"Previous Close":[],
			  "Open":[],
			  "Bid":[],
			  "Ask":[],
			  "Day&#x27;s Range":[],
			  "52 Week Range":[],
			  "Volume":[],
			  "Avg. Volume":[],
			  "Market Cap":[],
			  "Beta":[],
			  "PE Ratio (TMM)":[],
			  "EPS (TMM)":[],
			  "Earnings Date":[],
			  "Dividend &amp; Yield":[],
			  "Ex-Dividend Date":[],
			  "ly Target Est":[]}

htmlText = response.text
for  indicator in Indicators:
	 #break
     print(indicator)
     splitList = htmlText.split(indicator)
     afterFirstSplit = splitList[1].split("\">")[2]
     afterSecondSplit = afterFirstSplit.split("</span></td>")
     data = afterSecondSplit[0]
