import requests

url =  "http://finance.yahoo.com/quote/AAPL?p=AAPL"

response = requests.get(url)

# This dictionary will hold all the data that will go into PANDAS Framework

data = {}

# Transformed from an array to a dictionary for key value pairs
# Values are a in a list data structure

Indicators = {"Previous Close":[],
			  "Open":[],
			  "Bid":[],
			  "Ask":[],
			  "Day's Range":[],
			  "52 Week Range":[],
			  "Volume":[],
			  "Avg. Volume":[],
			  "Market Cap":[],
			  "Beta":[],
			  "PE Ratio (TMM)":[],
			  "EPS (TMM)":[],
			  "Earnings Date":[],
			  "Dividend & Yield":[],
			  "Ex-Dividend Date":[],
			  "ly Target Est":[]}

print(response)
print(response.status_code)

htmlText = response.text 
# print(htmlText)

# new construct to print out the parsed data in the Indicators dictionary
# this goes key and value looked for each value dynamically

for indicator in Indicators:
	break
	print(indicator)
	splitList = htmlText.split(indicator)
	afterFirstSplit = splitList[1].split("\">")[1]
	afterSecondSplit = afterFirstSplit.split("</td>")
	dataValue = afterSecondSplit[0]
	Indicators[indicator].append(dataValue)

print(Indicators)
