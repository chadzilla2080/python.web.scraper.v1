import requests

url =  "http://finance.yahoo.com/quote/AAPL?p=AAPL"

response = requests.get(url)

Indicators = ["Previous Close",
			  "Open",
			  "Bid",
			  "Ask",
			  "Day's Range",
			  "52 Week Range",
			  "Volume",
			  "Avg. Volume",
			  "Market Cap",
			  "Beta",
			  "PE Ratio (TMM)",
			  "EPS (TMM)",
			  "Earnings Date",
			  "Dividend & Yield",
			  "Ex-Dividend Date",
			  "ly Target Est"]

print(response)
print(response.status_code)

# First Response Page Capture
# htmlText = response.text
# print(response.text)

# First Split Finds Main Theme In HTML
# htmlText = response.text
# splitList = htmlText.split("Previous Close")
# print(response.text)

# Second Split Different Method

htmlText = response.text

splitList = htmlText.split("Previous Close")
print("Search To Find", splitList[1].split("\">"))
	
# Second Split After Splitting Garbage Text
# htmlText = response.text
# splitList = htmlText.split("Previous Close")
# afterFirstSplit = splitList[1].split("\">")[1]
# print(afterFirstSplit)

# Third Split Before Value
# htmlText = response.text
# splitList = htmlText.split("Previous Close")
# afterFirstSplit = splitList[1].split("\">")[2]
# afterSecondSplit = afterFirstSplit.split("</span></td>")
# data = afterSecondSplit[1]
# print(afterSecondSplit)

# Fourth Split

# htmlText = response.text
# splitList = htmlText.split("Previous Close")
# print(splitList[2])

#afterFirstSplit = splitList[1].split("\">")[1]
#afterSecondSplit = afterFirstSplit.split("</span></td>")
#print(afterSecondSplit)


